package com.example.springboot.demo.jpa.controller;

import com.example.springboot.demo.jpa.model_or_entity.Product;
import com.example.springboot.demo.jpa.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class productController {

    @Autowired
    private ProductService service;

@PostMapping("/addProduct")
    public Product addProduct(@RequestBody Product product)
    {
        return service.saveProduct(product);
    }
    @PostMapping("/addProducts")
    public List<Product> addProducts(@RequestBody List<Product> products)
    {
        return service.saveProducts(products);
    }
@PostMapping("/products")
    public List<Product> findAllProducts(){
    return service.getProducts();
    }
    @GetMapping("/product/{id}")
    public Product findProduct(@PathVariable int id){
    return service.getProductById(id);
    }
    @GetMapping("/product/{name}")
    public Product findProductByName(@PathVariable String name){
        return service.getProductByName(name);
    }
    @PutMapping("/update")
    public Product updateProduct(@RequestBody Product product)
    {
        return service.updateProduct(product);
    }
    @DeleteMapping("/delete/{id}")
    public String deleteProduct(@PathVariable int id){
    return service.deleteProduct(id);
    }
}
