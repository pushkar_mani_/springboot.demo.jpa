package com.example.springboot.demo.jpa.repository;

import com.example.springboot.demo.jpa.model_or_entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository <Product,Integer>{
    Product findByname(String name);
}
